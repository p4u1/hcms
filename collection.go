// Copyright (C) 2022 The hcms authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"context"
)

type Collection struct {
	Name   string
	Fields []Field
}

type Field struct {
	Name     string    `json:"name"`
	Type     FieldType `json:"type"`
	Listable bool      `json:"listable"`
}

type FieldType string

const (
	FieldTypeText           FieldType = "text"
	FieldTypeStructuredText FieldType = "structuredtext"
	FieldTypeNumber         FieldType = "number"
)

type CollectionStore interface {
	FindCollections(ctx context.Context) ([]Collection, error)
	FindCollectionByName(ctx context.Context, name string) (Collection, error)
	CreateCollection(ctx context.Context, c Collection) error
	UpdateCollection(ctx context.Context, name string, c Collection) error
}
