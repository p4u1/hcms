package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
)

type Router struct {
	store CollectionStore
}

func (router *Router) GetCollections(w http.ResponseWriter, r *http.Request) {
	collections, err := router.store.FindCollections(r.Context())
	if err != nil {
		writeError(w, err)
		return
	}
	writeJSON(w, collections)
}

func (router *Router) PostCollection(w http.ResponseWriter, r *http.Request) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		writeError(w, err)
		return
	}
	collection := Collection{}
	err = json.Unmarshal(data, &collection)
	if err != nil {
		writeError(w, err)
		return
	}
	err = router.store.CreateCollection(r.Context(), collection)
	if err != nil {
		writeError(w, err)
		return
	}
	writeJSON(w, collection)
}

func (router *Router) GetCollectionByName(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	collections, err := router.store.FindCollectionByName(r.Context(), name)
	if err != nil {
		writeError(w, err)
		return
	}
	writeJSON(w, collections)
}

func (router *Router) PatchCollectionByName(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	data, err := io.ReadAll(r.Body)
	if err != nil {
		writeError(w, err)
		return
	}
	collection := Collection{}
	err = json.Unmarshal(data, collection)
	if err != nil {
		writeError(w, err)
		return
	}
	err = router.store.UpdateCollection(r.Context(), name, collection)
	if err != nil {
		writeError(w, err)
		return
	}
	writeJSON(w, collection)
}

func writeError(w http.ResponseWriter, err error) {
	if IsBadInputError(err) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if IsNotFoundError(err) {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	log.Println(w)
	w.WriteHeader(http.StatusInternalServerError)
}

func writeJSON(w http.ResponseWriter, data any) {
	raw, err := json.Marshal(data)
	if err != nil {
		writeError(w, err)
		return
	}
	w.Write(raw)
}
