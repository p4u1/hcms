package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"sync"
)

// FileDB implements the CollectionStore and EntityStore. It stores the data in
// a json file.
type FileDB struct {
	path string

	m    *sync.RWMutex
	Data FileData
}

func (f *FileDB) write() error {
	return writeFileData(f.path, f.Data)
}

type FileData map[string]CollectionData

// readFileData reads FileData from the given path.
func readFileData(path string) (FileData, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	fileData := FileData{}
	err = json.Unmarshal(data, &fileData)
	if err != nil {
		return nil, fmt.Errorf("invalid db: %s", err)
	}
	return fileData, nil
}

// writeFileData writes FileData to the given path.
func writeFileData(path string, fileData FileData) error {
	data, err := json.Marshal(fileData)
	if err != nil {
		return fmt.Errorf("serialize db: %s", err)
	}
	err = os.WriteFile(path, data, 0666)
	if err != nil {
		return fmt.Errorf("write db: %s", err)
	}
	return nil
}

type CollectionData struct {
	Collection Collection `json:"collection"`
	Entities   []Entity   `json:"entities"`
}

func OpenFileDB(path string) (*FileDB, error) {
	fileData, err := readFileData(path)
	if err != nil {
		if os.IsNotExist(err) {
			err = writeFileData(path, FileData{})
			if err != nil {
				return nil, fmt.Errorf("write new db: %s", err)
			}
		} else {
			return nil, err
		}
	}
	return &FileDB{
		path: path,
		m:    &sync.RWMutex{},
		Data: fileData,
	}, nil
}

func (f *FileDB) FindCollections(ctx context.Context) ([]Collection, error) {
	f.m.RLock()
	defer f.m.RUnlock()

	collections := []Collection{}
	for _, c := range f.Data {
		collections = append(collections, c.Collection)
	}
	return collections, nil
}

func (f *FileDB) FindCollectionByName(ctx context.Context, name string) (Collection, error) {
	f.m.RLock()
	defer f.m.RUnlock()

	data, ok := f.Data[name]
	if !ok {
		return Collection{}, NotFoundError{Type: "Collection"}
	}
	return data.Collection, nil
}

func (f *FileDB) CreateCollection(ctx context.Context, c Collection) error {
	f.m.Lock()
	defer f.m.Unlock()

	if _, ok := f.Data[c.Name]; ok {
		return fmt.Errorf("name already exists: %s", c.Name)
	}

	f.Data[c.Name] = CollectionData{
		Collection: c,
		Entities:   []Entity{},
	}

	return f.write()
}

func (f *FileDB) UpdateCollection(ctx context.Context, name string, c Collection) error {
	f.m.Lock()
	defer f.m.Unlock()

	if _, ok := f.Data[name]; !ok {
		return NotFoundError{Type: "Collection"}
	}

	f.Data[c.Name] = CollectionData{
		Collection: c,
		Entities:   f.Data[name].Entities,
	}

	return f.write()
}

func (f *FileDB) FindEntities(ctx context.Context, collection string) ([]Entity, error) {
	f.m.RLock()
	defer f.m.RUnlock()

	data, ok := f.Data[collection]
	if !ok {
		return nil, NotFoundError{Type: "Collection"}
	}

	entities := []Entity{}
	for _, e := range data.Entities {
		entities = append(entities, e)
	}

	return entities, nil
}

func (f *FileDB) FindEntityByIndex(ctx context.Context, collection string, index int) (Entity, error) {
	f.m.RLock()
	defer f.m.RUnlock()

	data, ok := f.Data[collection]
	if !ok {
		return Entity{}, NotFoundError{Type: "Collection"}
	}
	if len(data.Entities) <= index {
		return Entity{}, NotFoundError{Type: "Entity"}
	}

	return data.Entities[index], nil
}

func (f *FileDB) CreateEntity(ctx context.Context, collection string, entity Entity) error {
	f.m.Lock()
	defer f.m.Unlock()

	data, ok := f.Data[collection]
	if !ok {
		return NotFoundError{Type: "Collection"}
	}

	f.Data[collection] = CollectionData{
		Collection: data.Collection,
		Entities:   append(data.Entities, entity),
	}

	return f.write()
}

func (f *FileDB) UpdateEntity(ctx context.Context, collection string, index int, entity Entity) error {
	f.m.Lock()
	defer f.m.Unlock()

	data, ok := f.Data[collection]
	if !ok {
		return NotFoundError{Type: "Collection"}
	}
	if len(data.Entities) <= index {
		return NotFoundError{Type: "Entity"}
	}

	data.Entities[index] = entity

	f.Data[collection] = CollectionData{
		Collection: data.Collection,
		Entities:   data.Entities,
	}

	return f.write()
}
