// Copyright (C) 2022 The hcms authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
)

func main() {
	if err := run(); err != nil {
		log.Println(err)
	}
}

func run() error {
	db, err := OpenFileDB("db.json")
	if err != nil {
		return err
	}

	filesController := &MediaController{MediaPath: "media"}
	webController := &WebController{collectionStore: db, entityStore: db}
	apiRouter := &Router{store: db}

	r := chi.NewRouter()
	r.Get("/", webController.RenderDashboard)
	r.Get("/dashboard", webController.RenderDashboard)

	r.Get("/collections", webController.RenderCollectionListPage)
	r.Get("/collections/new", webController.RenderCollectionCreatePage)
	r.Post("/collections/new", webController.HandlePostCollectionCreate)
	r.Get("/collections/{name}", webController.RenderCollectionUpdatePage)
	r.Post("/collections/{name}", webController.HandlePostCollectionUpdate)

	r.Get("/{collection}", webController.RenderCollectionPage)
	r.Get("/{collection}/new", webController.RenderEntityCreatePage)
	r.Post("/{collection}/new", webController.HandlePostEntityCreate)
	r.Get("/{collection}/{index}", webController.RenderEntityUpdatePage)
	r.Post("/{collection}/{index}", webController.HandlePostEntityUpdate)

	r.Post("/media", filesController.PostFile)
	r.Get("/media/{name}", filesController.GetFile)

	r.Route("/api/v1", func(r chi.Router) {
		r.Get("/collections", apiRouter.GetCollections)
		r.Post("/collections", apiRouter.PostCollection)
		r.Get("/collections/{name}", apiRouter.GetCollectionByName)
		r.Patch("/collections/{name}", apiRouter.PatchCollectionByName)
	})
	err = http.ListenAndServe(":4040", r)
	if err != nil {
		return err
	}
	return nil
}
