package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
)

type MediaController struct {
    MediaPath string
}

func (c *MediaController) PostFile(w http.ResponseWriter, r *http.Request) {
	r.Body = http.MaxBytesReader(w, r.Body, maxFileSize)

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	err := r.ParseMultipartForm(maxFileSize)
	if err != nil {
		writeError(w, err)
		return
	}
	// FormFile returns the first file for the given key `image`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, header, err := r.FormFile("image")
	if err != nil {
		writeError(w, err)
		return
	}
	defer file.Close()

	name := header.Filename

	f, err := os.OpenFile(fmt.Sprintf("%s/%s", c.MediaPath, name), os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()
	io.Copy(f, file)

	data, err := json.Marshal(&AddFileResponse{
		Success: 1,
		File: File{
			URL: fmt.Sprintf("http://localhost:4040/media/%s", name),
		},
	})
	if err != nil {
		writeError(w, err)
		return
	}
	w.Write(data)
}

const maxFileSize = 1 * 1024 * 1024 // 1Mb

type AddFileResponse struct {
	Success int  `json:"success"`
	File    File `json:"file"`
}

type File struct {
	URL string `json:"url"`
}

func (c *MediaController) GetFile(w http.ResponseWriter, r *http.Request) {
	http.StripPrefix("/media/", http.FileServer(http.Dir(c.MediaPath))).ServeHTTP(w, r)
}
