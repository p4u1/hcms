// Copyright (C) 2022 The hcms authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"embed"
	"encoding/json"
	"html/template"
	"log"
	"math"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
)

//go:embed assets
var assetsFS embed.FS

type WebController struct {
	collectionStore CollectionStore
	entityStore     EntityStore
}

func (c *WebController) RenderDashboard(w http.ResponseWriter, r *http.Request) {
	collections, err := c.collectionStore.FindCollections(r.Context())
	tpl, err := template.ParseFS(assetsFS, "assets/base.tpl", "assets/dashboard.tpl")
	if err != nil {
		log.Println(err)
		return
	}
	err = tpl.Execute(w, map[string]interface{}{
		"Error":       err,
		"Collections": collections,
	})
	if err != nil {
		log.Println(err)
	}
}

func (c *WebController) RenderCollectionListPage(w http.ResponseWriter, r *http.Request) {
	collections, err := c.collectionStore.FindCollections(r.Context())
	tpl, err := template.ParseFS(assetsFS, "assets/base.tpl", "assets/collection_list.tpl")
	if err != nil {
		log.Println(err)
		return
	}
	err = tpl.Execute(w, map[string]interface{}{
		"Error":       err,
		"Collections": collections,
	})
	if err != nil {
		log.Println(err)
	}
}

func (c *WebController) RenderCollectionCreatePage(w http.ResponseWriter, r *http.Request) {
	collections, err := c.collectionStore.FindCollections(r.Context())
	tpl, err := template.ParseFS(assetsFS, "assets/base.tpl", "assets/collection_form.tpl")
	if err != nil {
		// TODO: error handling
		log.Println(err)
		return
	}
	err = tpl.Execute(w, map[string]interface{}{
		"Collections": collections,
	})
	if err != nil {
		// TODO: error handling
		log.Println(err)
		return
	}
}

func (c *WebController) HandlePostCollectionCreate(w http.ResponseWriter, r *http.Request) {
	collection, err := parseCollectionFormData(r)
	if err != nil {
		// TODO: error handling
		log.Println(err)
		return
	}
	err = c.collectionStore.CreateCollection(r.Context(), collection)
	if err != nil {
		// TODO: error handling
		log.Println(err)
		return
	}
	c.RenderCollectionListPage(w, r)
}

func parseCollectionFormData(r *http.Request) (Collection, error) {
	err := r.ParseForm()
	if err != nil {
		return Collection{}, err
	}
	name := r.FormValue("name")
	fieldNames := r.Form["fieldName"]
	fieldTypes := r.Form["fieldType"]
	fieldListables := r.Form["fieldListable"]
	fields := []Field{}
	for i := 0; i < minLen(fieldNames, fieldTypes, fieldListables); i++ {
		fields = append(fields, Field{
			Name:     fieldNames[i],
			Type:     FieldType(fieldTypes[i]),
			Listable: fieldListables[i] == "true",
		})
	}
	return Collection{Name: name, Fields: fields}, nil
}

func (c *WebController) RenderCollectionUpdatePage(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	collections, err := c.collectionStore.FindCollections(r.Context())
	if err != nil {
		// TODO: error handling
		log.Println(err)
		return
	}
	collection, err := c.collectionStore.FindCollectionByName(r.Context(), name)
	if err != nil {
		// TODO: error handling
		log.Println(err)
		return
	}
	tpl, err := template.ParseFS(assetsFS, "assets/base.tpl", "assets/collection_form.tpl")
	if err != nil {
		// TODO: error handling
		log.Println(err)
		return
	}
	err = tpl.Execute(w, map[string]interface{}{
		"Collection":  collection,
		"Collections": collections,
	})
	if err != nil {
		// TODO: error handling
		log.Println(err)
		return
	}
}

func (c *WebController) HandlePostCollectionUpdate(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	collection, err := parseCollectionFormData(r)
	if err != nil {
		// TODO: error handling
		log.Println(err)
		return
	}
	err = c.collectionStore.UpdateCollection(r.Context(), name, collection)
	if err != nil {
		// TODO: error handling
		log.Println(err)
		return
	}
	c.RenderCollectionListPage(w, r)
}

func (c *WebController) RenderCollectionPage(w http.ResponseWriter, r *http.Request) {
	collectionName := chi.URLParam(r, "collection")
	collection, err := c.collectionStore.FindCollectionByName(r.Context(), collectionName)
	if err != nil {
		// TODO: handle error
		return
	}
	entities, _ := c.entityStore.FindEntities(r.Context(), collection.Name)
	if err != nil {
		// TODO: handle error
		return
	}
	collections, err := c.collectionStore.FindCollections(r.Context())
	tpl, err := template.ParseFS(assetsFS, "assets/base.tpl", "assets/collection_view.tpl")
	if err != nil {
		log.Println(err)
		return
	}
	err = tpl.Execute(w, map[string]interface{}{
		"Collection":  collection,
		"Entities":    entities,
		"Collections": collections,
	})
	if err != nil {
		log.Println(err)
	}
}

func (c *WebController) RenderEntityCreatePage(w http.ResponseWriter, r *http.Request) {
	collectionName := chi.URLParam(r, "collection")
	collection, err := c.collectionStore.FindCollectionByName(r.Context(), collectionName)
	if err != nil {
		// TODO: handle error
		return
	}
	collections, err := c.collectionStore.FindCollections(r.Context())
	tpl, err := template.ParseFS(assetsFS, "assets/base.tpl", "assets/entity_form.tpl")
	if err != nil {
		log.Println(err)
		return
	}
	err = tpl.Execute(w, map[string]interface{}{
		"CollectionName":   collection.Name,
		"CollectionFields": collection.Fields,
		"Collections":      collections,
		"Entity":           Entity{Fields: map[string]any{}},
		"marshal": func(v interface{}) template.JS {
			a, _ := json.Marshal(v)
			return template.JS(a)
		},
	})
	if err != nil {
		log.Println(err)
	}
}

func (c *WebController) HandlePostEntityCreate(w http.ResponseWriter, r *http.Request) {
	collectionName := chi.URLParam(r, "collection")
	collection, err := c.collectionStore.FindCollectionByName(r.Context(), collectionName)
	if err != nil {
		// TODO: handle error
		log.Println(err)
		return
	}

	e, err := parseEntityFormData(r, collection.Fields)
	if err != nil {
		log.Println(err)
		return
	}

	err = c.entityStore.CreateEntity(r.Context(), collection.Name, e)
	if err != nil {
		log.Println(err)
		return
	}
	c.RenderCollectionPage(w, r)
}

func parseEntityFormData(r *http.Request, fields []Field) (Entity, error) {
	err := r.ParseForm()
	if err != nil {
		return Entity{}, err
	}
	e := Entity{Fields: map[string]any{}}
	for _, field := range fields {
		v := r.FormValue(field.Name)
		switch field.Type {
		case FieldTypeText:
			e.Fields[field.Name] = v
		case FieldTypeStructuredText:
			if v != "" {
				d := map[string]interface{}{}
				err := json.Unmarshal([]byte(v), &d)
				if err != nil {
					return Entity{}, err
				}
				e.Fields[field.Name] = d
			}
		case FieldTypeNumber:
			n, err := strconv.Atoi(v)
			if err != nil {
				return Entity{}, err
			}
			e.Fields[field.Name] = n
		}
	}
	return e, nil
}

func (c *WebController) RenderEntityUpdatePage(w http.ResponseWriter, r *http.Request) {
	collectionName := chi.URLParam(r, "collection")
	collection, err := c.collectionStore.FindCollectionByName(r.Context(), collectionName)
	if err != nil {
		// TODO: handle error
		log.Println("collection: ", err)
		return
	}
	entityIndexRaw := chi.URLParam(r, "index")
	entityIndex, err := strconv.Atoi(entityIndexRaw)
	if err != nil {
		// TODO: handle error
		log.Println(err)
		return
	}
	entity, err := c.entityStore.FindEntityByIndex(r.Context(), collectionName, entityIndex)
	if err != nil {
		// TODO: handle error
		log.Println("entity: ", err)
		return
	}
	collections, err := c.collectionStore.FindCollections(r.Context())
	tpl, err := template.ParseFS(assetsFS, "assets/base.tpl", "assets/entity_form.tpl")
	if err != nil {
		log.Println(err)
		return
	}
	err = tpl.Execute(w, map[string]interface{}{
		"Update":           true,
		"CollectionName":   collection.Name,
		"CollectionFields": collection.Fields,
		"Collections":      collections,
		"Entity":           entity,
		"Index":            entityIndex,
		"marshal": func(v interface{}) template.JS {
			a, _ := json.Marshal(v)
			return template.JS(a)
		},
	})
	if err != nil {
		log.Println(err)
	}
}

func (c *WebController) HandlePostEntityUpdate(w http.ResponseWriter, r *http.Request) {
	collectionName := chi.URLParam(r, "collection")
	collection, err := c.collectionStore.FindCollectionByName(r.Context(), collectionName)
	if err != nil {
		// TODO: handle error
		log.Println(err)
		return
	}
	entityIndexRaw := chi.URLParam(r, "index")
	entityIndex, err := strconv.Atoi(entityIndexRaw)
	if err != nil {
		// TODO: handle error
		log.Println(err)
		return
	}

	e, err := parseEntityFormData(r, collection.Fields)
	if err != nil {
		// TODO: handle error
		log.Println(err)
		return
	}

	err = c.entityStore.UpdateEntity(r.Context(), collection.Name, entityIndex, e)
	if err != nil {
		log.Println(err)
		return
	}
	c.RenderCollectionPage(w, r)
}

func minLen(a, b, c []string) int {
	return int(
		math.Min(
			math.Min(float64(len(a)), float64(len(b))),
			float64(len(c)),
		),
	)
}
