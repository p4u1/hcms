{{ define "content" }}
    <a class="ui button" href="/collections/new">New Collection</a>
    <table class="ui celled table">
      <thead>
        <tr>
            <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {{ range .Collections }}
        <tr>
          <td data-label="Name">{{ .Name }}</td>
        </tr>
        {{ end }}
      </tbody>
    </table>
{{ end }}
