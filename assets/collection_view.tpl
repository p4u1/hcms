{{ define "content" }}
    <a class="ui button" href="/{{ .Collection.Name }}/new">New {{ .Collection.Name }}</a>
    <table class="ui celled table">
      <thead>
        <tr>
            {{ range .Collection.Fields }}
                {{ if eq .Listable true }}
                    <th data-label="{{ .Name }}">{{ .Name }}</th>
                {{ end }}
            {{ end }}
            <th></th>
        </tr>
      </thead>
      <tbody>
        {{ range $index, $entity := .Entities }}
            <tr>
                {{ range $field := $.Collection.Fields }}
                    {{ if eq .Listable true }}
                        <td data-label="{{ $field.Name }}">{{ index $entity.Fields $field.Name }}</td>
                    {{ end }}
                {{ end }}
                <td><a class="ui button" href="/{{ $.Collection.Name }}/{{ $index }}">Edit</a></td>
            </tr>
        {{ end }}
      </tbody>
    </table>
{{ end }}
