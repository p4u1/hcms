{{ define "content" }}
    <form class="ui form" method="POST" action="/{{ .CollectionName }}/{{ if .Update }}{{ .Index }}{{ else }}new{{ end }}">
        {{ range .CollectionFields }}
            {{ if eq .Type "text" }}
                {{$data := (index $.Entity.Fields .Name)}}
                <div class="field">
                    <label for="{{ .Name }}">{{ .Name }}</label>
                    <input type="text" name="{{ .Name }}" placeholder="{{ .Name }}" value="{{ if $data }}{{ $data}}{{ end }}">
                </div>
            {{ end }}
            {{ if eq .Type "structuredtext" }}
                {{$data := (index $.Entity.Fields .Name)}}
                <div class="field">
                    <label>{{ .Name }}</label>
                    <div id="{{ .Name }}"></div>
                    <input type="hidden" name="{{ .Name }}" id="{{ .Name }}input" value="{{ if $data }}{{ call $.marshal $data}}{{ end }}"/>
                </div>
                <script>
                    (() => {
                        const ImageTool = window.ImageTool;
                        const editor = new EditorJS({
                           /**
                            * Id of Element that should contain Editor instance
                            */
                           holder: '{{ .Name }}',
                           tools: {
                               image: {
                                   class: ImageTool,
                                   config: {
                                      endpoints: {
                                         byFile: 'http://localhost:4040/media',
                                      }
                                   }
                               }
                           },
                           onReady: () => {
                                {{ if $data }}
                                   console.log("{{ call $.marshal $data }}");
                                    editor.render(JSON.parse("{{ call $.marshal $data }}"));
                                {{ end }}
                           },
                           onChange: (api, event) => {
                               const input = document.getElementById('{{ .Name }}input');
                               editor.save().then(data => {
                                   input.value = JSON.stringify(data);
                               });
                           }
                         });
                     })();
                </script>
            {{ end }}
            {{ if eq .Type "number" }}
                {{$data := (index $.Entity.Fields .Name)}}
                <div class="field">
                    <label for="{{ .Name }}">{{ .Name }}</label>
                    <input type="text" name="{{ .Name }}" placeholder="{{ .Name }}" value="{{ if $data }}{{ $data}}{{ end }}">
                </div>
            {{ end }}
        {{ end }}
      <button class="ui button" type="submit">Submit</button>
    </form>
{{ end }}
