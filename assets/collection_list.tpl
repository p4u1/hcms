{{ define "content" }}
    <a class="ui button" href="/collections/new">New Collection</a>
    <table class="ui celled table">
        <thead>
            <tr>
                <th>Name</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {{ range .Collections }}
                <tr>
                    <td data-label="Name">{{ .Name }}</td>
                    <td><a class="ui button" href="/collections/{{ .Name }}">Edit</a></td>
                </tr>
            {{ end }}
        </tbody>
    </table>
{{ end }}
