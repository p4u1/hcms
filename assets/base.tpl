<html>
    <head>
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.css">
        <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@editorjs/editorjs@latest"></script>
        <script src="https://cdn.jsdelivr.net/npm/@editorjs/image@2.3.0"></script>
    </head>
    <body>
        <div class="ui internally celled grid">
          <div class="three wide column">
            <div class="ui vertical menu">
              <div  class="item">
                <a href="/collections">Collections</a>
                <div class="menu">
                  {{ range .Collections }}
                      <a class="item" href="/{{ .Name }}">{{ .Name }}</a>
                  {{ end }}
                </div>
              </div>
            </div>
          </div>
          <div class="ten wide column">
            {{ template "content" . }}
          </div>
        </div>
    </body>
</html>
