{{ define "content" }}
    <script>
        function getRandomInt(max) {
            return Math.floor(Math.random() * max);
        }
        const onChangeListable = (el) => {
            document.getElementById(`${el.dataset.fieldid}`).value = el.checked ? 'true' : 'false'
        };

        function addField() {
            const fieldid = getRandomInt(100)
            const frag = document.createRange().createContextualFragment(`
                <div class="field">
                    <label for="fieldName">Name</label>
                    <input type="text" name="fieldName" placeholder="Field Name">
                    <label for="fields">Type</label>
                    <select name="fieldType">
                      <option value="text" default>Text</option>
                      <option value="structuredtext">Structured Text</option>
                      <option value="number">Number</option>
                    </select>
                    <label for="fieldListable">Listable</label>
                    <input type="hidden" name="fieldListable" id="${fieldid}" value="true" />
                    <input
                        data-id="${fieldid}"
                        type="checkbox"
                        checked
                        onChange="onChangeListable(this)"
                    />
                </div>
            `);
            const addFieldButton = document.getElementById("addFieldButton");
            addFieldButton.parentNode.insertBefore(frag, addFieldButton);
        }
    </script>
    <form class="ui form" method="POST" action="/collections/{{ if .Collection }}{{ .Collection.Name }}{{ else }}new{{ end }}">
        <div class="field">
            <label for="name">Name</label>
            <input type="text" name="name" placeholder="Name" value={{ .Collection.Name }}>
        </div>
        <div class="field">
            {{ range .Collection.Fields }}
                <div class="field">
                    <label for="fieldName">Name</label>
                    <input type="text" name="fieldName" placeholder="Field Name" value={{ .Name }}>
                    <label for="fields">Type</label>
                    <select name="fieldType">
                      <option value="text" {{ if eq .Type "text"}}default{{ end }}>Text</option>
                      <option value="structuredtext" {{ if eq .Type "structuredtext"}}default{{ end }}>Structured Text</option>
                      <option value="number" {{ if eq .Type "number"}}default{{ end }}>Number</option>
                    </select>
                    <label for="fieldListable">Listable</label>
                    <input type="hidden" name="fieldListable" id="field{{ .Name }}" value="{{ .Listable }}" />
                    <input
                        data-fieldid="field{{ .Name }}"
                        type="checkbox"
                        {{ if eq .Listable true}}checked{{ end }}
                        onChange="onChangeListable(this)"
                    />
                </div>
            {{ end }}
            <button class="ui button" id="addFieldButton" type="button" onClick="addField()">Add Field</button>
        </div>
<button class="ui button" type="submit">Submit</button>
    </form>
{{ end }}
