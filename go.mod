module codeberg.org/p4u1/hcms

go 1.18

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/google/uuid v1.3.0
)
