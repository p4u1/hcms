// Copyright (C) 2022 The hcms authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import "fmt"

type NotFoundError struct {
	Type string
}

func (n NotFoundError) Error() string {
	return fmt.Sprintf("%s not found", n.Type)
}

func IsNotFoundError(err error) bool {
	if err == nil {
		return false
	}
	_, ok := err.(NotFoundError)
	return ok
}

type BadInputError struct {
	Err string
}

func (b BadInputError) Error() string {
	return b.Err
}

func IsBadInputError(err error) bool {
	if err == nil {
		return false
	}
	_, ok := err.(BadInputError)
	return ok
}
